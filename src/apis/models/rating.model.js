const mongoose = require('mongoose')

const ratingSchema = mongoose.Schema(
    {
        rating: { type: Number, require: true },
        user: {
            type: String,
            ref: 'User',
            require: true,
        },
    },
    {
        timestamps: true,
    }
)
module.exports = mongoose.model('Rating', ratingSchema)
