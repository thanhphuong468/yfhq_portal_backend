const express = require('express')
const { adminController } = require('../../controllers')
const auth = require('../../../middlewares/auth')

const router = express.Router()

router.get('/getInfoHome', auth, adminController.getInfoHome)

module.exports = router
