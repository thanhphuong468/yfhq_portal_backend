const express = require('express')
const auth = require('../../../middlewares/auth')
const { ratingController } = require('../../controllers')

const router = express.Router()

router.post('/create', auth, ratingController.addRating)

module.exports = router
