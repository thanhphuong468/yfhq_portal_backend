const Activitys = require('../models/activity.model')
const ApiError = require('../../utils/api-error')
const e = require('express')
const { Activity } = require('../models')
const { User } = require('../models')
const { createUser } = require('./user.service')
const moment = require('moment')

const getAllListActivities = async () => {
    if (req.query?.startDate !== '' && req.query?.endDate !== '') {
        let dataStartDate = new Date(req.query.startDate)
        let dataEndDate = new Date(req.query.endDate)
        return Promise.all([
            Activitys.find({ tag: 'lanh-dao-kinh-doanh', startDate: { $gte: dataStartDate }, endDate: { $lt: dataEndDate } })
                .limit(8)
                .sort({ createdDate: -1 }),
            Activitys.find({ tag: 'dao-duc', startDate: { $gte: dataStartDate }, endDate: { $lt: dataEndDate } })
                .limit(8)
                .sort({ createdDate: -1 }),
            Activitys.find({ tag: 'hoc-tap', startDate: { $gte: dataStartDate }, endDate: { $lt: dataEndDate } })
                .limit(5)
                .sort({ createdDate: -1 }),
            Activitys.find({ tag: 'the-luc', startDate: { $gte: dataStartDate }, endDate: { $lt: dataEndDate } })
                .limit(8)
                .sort({ createdDate: -1 }),
            Activitys.find({ tag: 'tinh-nguyen', startDate: { $gte: dataStartDate }, endDate: { $lt: dataEndDate } })
                .limit(8)
                .sort({ createdDate: -1 }),
            Activitys.find({ tag: 'hoi-nhap', startDate: { $gte: dataStartDate }, endDate: { $lt: dataEndDate } })
                .limit(8)
                .sort({ createdDate: -1 }),
            Activitys.find({ tag: 'khac', startDate: { $gte: dataStartDate }, endDate: { $lt: dataEndDate } })
                .limit(8)
                .sort({ createdDate: -1 }),
        ])
    } else {
        return Promise.all([
            Activitys.find({ tag: 'lanh-dao-kinh-doanh' }).limit(8).sort({ createdDate: -1 }),
            Activitys.find({ tag: 'dao-duc' }).limit(8).sort({ createdDate: -1 }),
            Activitys.find({ tag: 'hoc-tap' }).limit(5).sort({ createdDate: -1 }),
            Activitys.find({ tag: 'the-luc' }).limit(8).sort({ createdDate: -1 }),
            Activitys.find({ tag: 'tinh-nguyen' }).limit(8).sort({ createdDate: -1 }),
            Activitys.find({ tag: 'hoi-nhap' }).limit(8).sort({ createdDate: -1 }),
            Activitys.find({ tag: 'khac' }).limit(8).sort({ createdDate: -1 }),
        ])
    }
}
const getLisActivitiesByPage = async (req) => {
    var page = req.query.page
    var limit = req.query.limit
    var slug = req.query.slug
    let listActivity = []
    if (page < 1) {
        page = 1
    }
    if (req.query?.startDate !== '' && req.query?.endDate !== '') {
        let dataStartDate = new Date(req.query.startDate)
        let dataEndDate = new Date(req.query.endDate)
        if (slug) {
            var skipPage = (parseInt(page) - 1) * parseInt(limit)
            listActivity = await Activitys.find({
                tag: slug,
                startDate: { $gte: dataStartDate },
                endDate: { $lt: dataEndDate },
            })
                .sort({ createdDate: -1 })
                .skip(skipPage)
                .limit(parseInt(limit))
        } else {
            var skipPage = (parseInt(page) - 1) * parseInt(limit)
            listActivity = await Activitys.find({
                startDate: { $gte: dataStartDate },
                endDate: { $lt: dataEndDate },
            })
                .sort({ createdDate: -1 })
                .skip(skipPage)
                .limit(parseInt(limit))
        }
    } else {
        if (slug) {
            var skipPage = (parseInt(page) - 1) * parseInt(limit)
            return Activitys.find({ tag: slug }).sort({ createdDate: -1 }).skip(skipPage).limit(parseInt(limit))
        } else {
            var skipPage = (parseInt(page) - 1) * parseInt(limit)
            return Activitys.find({}).sort({ createdDate: -1 }).skip(skipPage).limit(parseInt(limit))
        }
    }
    return listActivity
}
const getActivitiesByTag = async (req) => {
    if (req.query?.startDate !== '' && req.query?.endDate !== '') {
        let dataStartDate = new Date(req.query.startDate)
        let dataEndDate = new Date(req.query.endDate)
        return Activitys.find({ tag: req.query.tag, startDate: { $gte: dataStartDate }, endDate: { $lt: dataEndDate } })
            .sort({ createdDate: -1 })
            .limit(3)
    } else {
        return Activitys.find({ tag: req.query.tag }).sort({ createdDate: -1 }).limit(3)
    }
}

const getActivity = async (req) => {
    return Activitys.findOne({ slug: req.params.slug })
}

const getTotalActivitesForAdmin = async () => {
    return Activitys.count({ status: true })
}

const getTotalActivities = async (req, res) => {
    var slug = req.query.slug
    if (req.query?.startDate !== '' && req.query?.endDate !== '') {
        console.log('vo co truyen')
        let dataStartDate = new Date(req.query.startDate)
        let dataEndDate = new Date(req.query.endDate)
        if (slug === undefined) {
            return Activitys.find({
                startDate: { $gte: dataStartDate },
                endDate: { $lt: dataEndDate },
            }).count({})
        }
        return Activitys.find({
            startDate: { $gte: dataStartDate },
            endDate: { $lt: dataEndDate },
        }).count({ tag: slug })
    } else {
        console.log(slug)
        if (slug === undefined) {
            return Activitys.count({})
        }
        return Activitys.count({ tag: slug })
    }
}
const addActivity = async (req, res) => {
    const formData = req.body
    if (req.userId === '61c45900eb10b1a42261c1ce') {
        formData.status = true
    }
    formData.createdDate = Date.now()
    const newActivity = new Activitys(formData)
    return newActivity.save()
}
const AddregisterActivityForStudent = async (req, res) => {
    const idUser = req.body.idUserRegister
    idUser.createdDate = Date.now({})
    const userRegister = { idUserRegister: idUser }
    return Activity.findOneAndUpdate(
        { _id: req.body.Id_Activity },
        { $push: { participatingList: { $each: [userRegister], $position: 0 } } }
    )
}
const AddregisterUserForStudent = async (req, res) => {
    const idUser = req.body.idUserRegister
    const idActivity = req.body.Id_Activity
    idUser.createdDate = Date.now({})
    const activityRegister = { id_Activity: idActivity }
    const checkListAtivity = await User.findOne({ _id: idUser }).select('listActivity')
    for (let i = 0; i < checkListAtivity.listActivity.length; i++) {
        if (checkListAtivity.listActivity[i].id_Activity === idActivity) return 0
    }
    return User.findOneAndUpdate(
        { _id: req.body.idUserRegister },
        { $push: { listActivity: { $each: [activityRegister], $position: 0 } } }
    )
}
const getListActivityByUser = async (req, res) => {
    var page = req.query.page
    var limit = req.query.limit

    const userCurrent = await User.findOne({ _id: req.query.idUser }).select('listActivity')
    if (page < 1) {
        page = 1
    }
    var skipPage = (parseInt(page) - 1) * parseInt(limit)
    var start = parseInt(page) - 1 === 0 ? 0 : skipPage
    var end = start + parseInt(limit)
    var listIdActivity = []
    var listActivity = []
    if (req.query?.startDate !== '' && req.query?.endDate !== '') {
        let dataStartDate = new Date(req.query.startDate)
        let dataEndDate = new Date(req.query.endDate)
        for (let i = start; i < end; i++) {
            if (i < userCurrent.listActivity.length) listIdActivity.push(userCurrent.listActivity[i])
        }
        for (let j = 0; j < listIdActivity.length; j++) {
            let result = await Activity.findOne({ _id: listIdActivity[j].id_Activity })
            let checkStartDate = new Date(result?.startDate)
            let checkEndDate = new Date(result?.endDate)
            if (checkStartDate > dataStartDate && checkEndDate < dataEndDate) {
                listActivity.push({
                    checking: listIdActivity[j].checking,
                    infoActivity: result,
                })
            }
        }
    } else {
        for (let i = start; i < end; i++) {
            if (i < userCurrent.listActivity.length) listIdActivity.push(userCurrent.listActivity[i])
        }
        for (let j = 0; j < listIdActivity.length; j++) {
            let result = await Activity.findOne({ _id: listIdActivity[j].id_Activity })
            listActivity.push({
                checking: listIdActivity[j].checking,
                infoActivity: result,
            })
        }
    }

    return listActivity
}
const getTotalActivitiesForUser = async (req, res) => {
    const total = await User.findOne({ _id: req.query.idUser }).select('listActivity')
    console.log(total)
    return total.listActivity.length
}
const editActivity = async (req, res) => {
    const { idActivity, ...formData } = req.body
    if (req.userId === '61c45900eb10b1a42261c1ce') {
        formData.status = true
    }
    // const newActivity = new Activitys(formData)
    return await Activitys.updateOne({ _id: idActivity }, formData, { new: true })
}
const deleteActivity = async (req) => {
    return await Activitys.delete({ _id: req.params.id })
}
const getActivityForAdmin = async (status) => {
    if (status) {
        if (status?.deleted === 'true') {
            return await Activitys.findDeleted()
        }
        return await Activitys.find(status)
    }
    return await Activitys.find({ status: true })
}
const getTotalRequestActivityForAdmin = async () => {
    return await Activitys.count({ status: false })
}

const getListAttendance = async (req) => {
    return await Activitys.findOne({ slug: req.params.slug }).populate('participatingList.idUserRegister')
}
const getActivitiesByUserBase = async (id) => {
    return await Activitys.find({ userCreateId: id })
}

const getActivitiesForWeek = async () => {
    return await Activitys.find({
        createdAt: {
            $gte: new Date(new Date() - 7 * 60 * 60 * 24 * 1000),
        },
    }).count()
}

const analyticsActivities = async (data) => {
    let result = []
    if (data === undefined) {
        const dateNow = new Date()
        for (let i = 0; i < 7; i++) {
            let start = new Date(dateNow - i * 60 * 60 * 24 * 1000)
            start.setHours(0, 0, 0, 0)
            let end = new Date(dateNow - i * 60 * 60 * 24 * 1000)
            end.setHours(24, 0, 0, 0)

            const a = await Activitys.find({
                $and: [{ createdAt: { $gte: start } }, { createdAt: { $lt: end } }],
            }).count()
            result = [...result, a]
        }
    } else {
        let startDate = moment(data[0])
        let endDate = moment(data[1]).subtract(1, 'days')
        let days = endDate.diff(startDate, 'days')
        for (let i = 0; i < days + 1; i++) {
            let start = new Date(endDate - i * 60 * 60 * 24 * 1000)
            start.setHours(0, 0, 0, 0)
            let end = new Date(endDate - i * 60 * 60 * 24 * 1000)
            end.setHours(24, 0, 0, 0)

            const a = await Activitys.find({
                $and: [{ createdAt: { $gte: start } }, { createdAt: { $lt: end } }],
            }).count()
            result = [...result, a]
        }
    }
    return result
}
const getActivityStudentAttendance = async (req) => {
    let listStudentRegister = []
    const dataList = await Activitys.findOne({ slug: req.params.slug }).populate('participatingList.idUserRegister')
    const id_Activity = dataList._id
    if (dataList.participatingList) {
        dataList.participatingList.map((studentInfo) => {
            {
                studentInfo.idUserRegister?.listActivity?.map((item) => {
                    if (item.id_Activity == id_Activity) {
                        if (item.checking === 'true') {
                            let itemStudent = {
                                name: studentInfo?.idUserRegister?.displayName || '',
                                mssv: studentInfo?.idUserRegister?.email.split('@')[0] || 00000000,
                            }
                            listStudentRegister = [...listStudentRegister, itemStudent]
                        }
                    }
                })
            }
        })
    }

    return listStudentRegister
}

const getActivityStudentRegister = async (req) => {
    let listStudentRegister = []
    const dataList = await Activitys.findOne({ slug: req.params.slug }).populate('participatingList.idUserRegister')
    if (dataList.participatingList) {
        dataList.participatingList.map((item) => {
            {
                let itemStudent = {
                    name: item?.idUserRegister?.displayName || '',
                    mssv: item?.idUserRegister?.email.split('@')[0] || 00000000,
                }
                listStudentRegister = [...listStudentRegister, itemStudent]
            }
        })
    }

    return listStudentRegister
}

const restoreActivityById = async (id) => {
    return await Activitys.restore({ _id: id })
}
module.exports = {
    getAllListActivities,
    getLisActivitiesByPage,
    getActivitiesByTag,
    getActivity,
    getTotalActivities,
    addActivity,
    AddregisterActivityForStudent,
    AddregisterUserForStudent,
    getListActivityByUser,
    getTotalActivitiesForUser,
    editActivity,
    deleteActivity,
    getListAttendance,
    getActivityForAdmin,
    getTotalRequestActivityForAdmin,
    getTotalActivitesForAdmin,
    getActivitiesForWeek,
    analyticsActivities,
    getActivitiesByUserBase,
    getActivityStudentAttendance,
    getActivityStudentRegister,
    restoreActivityById,
}
