const Rating = require('../models/rating.model')

const ratingApp = async (req, res) => {
    const data = req.body
    data.user = req.userId
    const newRating = new Rating(data)
    return newRating.save()
}

module.exports = {
    ratingApp,
}
