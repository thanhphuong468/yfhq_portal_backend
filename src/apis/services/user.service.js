const httpStatus = require('http-status')
const moment = require('moment')
const bcrypt = require('bcryptjs')
const ApiError = require('../../utils/api-error')
const { User } = require('../models')
const { replaceOne } = require('../models/token.model')

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (userBody) => {
    console.log(userBody)
    if (await User.isEmailTaken(userBody.email)) {
        throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken')
    }
    return User.create(userBody)
}

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
    return User.findOne({ email })
}

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (id) => {
    return User.findById(id)
}

const updateUserById = async (id, data) => {
    return await User.findOneAndUpdate({ _id: id }, data, { new: true })
}

const updatePassword = async (id, data) => {
    return await User.findOneAndUpdate({ _id: id }, data, { new: true })
}

const getUsersForAdmin = async (role) => {
    if (role) {
        return await User.find({ role: role })
    }
    return await User.find({ role: 'user:unionBase' })
    // const data = await User.find({})
    // const new_arr = data.filter((data) => data.role !== 'admin')
    // return new_arr
}

const deleteUsers = async (req) => {
    return await User.delete({ _id: req.params.id })
}

const updateUsersForAdmin = async (req) => {
    const data = req.body
    if (req.userId === '61c45900eb10b1a42261c1ce') {
        data.status = true
    }
    return await User.updateOne({ _id: data._id }, data, { new: true })
}

const checkingUser = async (req, res) => {
    const { idUser, checking, idActivity } = req.body
    const state = await User.findOne({ _id: idUser, 'listActivity.id_Activity': idActivity })
    if (state === null) {
        return 'none'
    }
    const value = checking === 'true' ? 'false' : 'true'
    return await User.updateOne(
        { _id: idUser, 'listActivity.id_Activity': idActivity },
        { $set: { 'listActivity.$.checking': value } },
        { new: true }
    )
}
const getUserEditForAdmin = async (req) => {
    return User.findOne({ _id: req.params.id })
}

const updateInfoUserEditAdmin = async (req) => {
    const { idUser, ...formData } = req.body
    return await User.updateOne({ _id: idUser }, formData, { new: true })
}

const addUsers = async (req) => {
    const formData = req.body
    const newUser = new User(formData, { displayName: req.body.fullname })
    const user = await createUser(newUser)
    return user
}

const getTotalUserForAdmin = async () => {
    return User.count({})
}

const getUsersForWeek = async () => {
    return await User.find({
        createdAt: {
            $gte: new Date(new Date() - 7 * 60 * 60 * 24 * 1000),
        },
    }).count()
}

const analyticsUsers = async (data) => {
    let result = []
    if (data === undefined) {
        const dateNow = new Date()
        for (let i = 0; i < 7; i++) {
            let start = new Date(dateNow - i * 60 * 60 * 24 * 1000)
            start.setHours(0, 0, 0, 0)
            let end = new Date(dateNow - i * 60 * 60 * 24 * 1000)
            end.setHours(24, 0, 0, 0)

            const a = await User.find({
                $and: [{ createdAt: { $gte: start } }, { createdAt: { $lt: end } }],
            }).count()
            result = [...result, a]
        }
    } else {
        let startDate = moment(data[0])
        let endDate = moment(data[1]).subtract(1, 'days')
        let days = endDate.diff(startDate, 'days')
        for (let i = 0; i < days + 1; i++) {
            let start = new Date(endDate - i * 60 * 60 * 24 * 1000)
            start.setHours(0, 0, 0, 0)
            let end = new Date(endDate - i * 60 * 60 * 24 * 1000)
            end.setHours(24, 0, 0, 0)

            const a = await User.find({
                $and: [{ createdAt: { $gte: start } }, { createdAt: { $lt: end } }],
            }).count()
            result = [...result, a]
        }
    }
    return result
}

const resetPassword = async (id) => {
    const hashPassword = await bcrypt.hash('hcmute123', 10)
    const newUser = await User.findOneAndUpdate({ _id: id }, { password: hashPassword }, { new: true })
    return newUser
}

module.exports = {
    checkingUser,
    createUser,
    getUserByEmail,
    getUserById,
    updateUserById,
    getUsersForAdmin,
    updateUsersForAdmin,
    deleteUsers,
    getUserEditForAdmin,
    updateInfoUserEditAdmin,
    addUsers,
    getTotalUserForAdmin,
    getUsersForWeek,
    analyticsUsers,
    updatePassword,
    resetPassword,
}
