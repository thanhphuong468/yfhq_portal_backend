const { ratingService } = require('../services')

const addRating = async (req, res) => {
    ratingService
        .ratingApp(req, res)
        .then((data) => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

module.exports = {
    addRating,
}
