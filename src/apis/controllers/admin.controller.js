const httpStatus = require('http-status')
const { newsService, activityService, userService } = require('../services')

const getInfoHome = async (req, res) => {
    if (!req.userId) return res.json({ message: 'Unauthenticated' })
    const totalNews = await newsService.getTotalNewsForAdmin()
    const newForWeek = await newsService.getNewNewsForWeek()
    const totalActivities = await activityService.getTotalActivitesForAdmin()
    const activitiesForWeek = await activityService.getActivitiesForWeek()
    const totalUsers = await userService.getTotalUserForAdmin()
    const usersForWeek = await userService.getUsersForWeek()

    const analyticsNews = await newsService.analyticsNews(req.query.data)
    const analyticsUsers = await userService.analyticsUsers(req.query.data)
    const analyticsActivities = await activityService.analyticsActivities(req.query.data)

    res.send({
        totalNews,
        totalActivities,
        newForWeek,
        activitiesForWeek,
        totalUsers,
        usersForWeek,
        analyticsNews,
        analyticsUsers,
        analyticsActivities,
    })
}

module.exports = {
    getInfoHome,
}
